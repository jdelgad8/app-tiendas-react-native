import React, { Component } from "react";
import Auth from "./navigation/Auth";
import { Provider } from "react-redux";
import store from "./redux/store/store";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Auth />
      </Provider>
    );
  }
}
