import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Feather";

export default class NavBar extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.tabBar}>
        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigate("Dash")}
        >
          <Icon name="home" style={styles.tabIcon} />
          <Text style={styles.tabTitle}> Inicio</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigate("ListProducts")}
        >
          <Icon name="box" style={styles.tabIcon} />
          <Text style={styles.tabTitle}> Inventario</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.tabCart}
          onPress={() => navigate("ListProductSales")}
        >
          <Icon name="shopping-cart" style={styles.tabCartIcon} />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigate("Sales")}
        >
          <Icon name="trending-up" style={styles.tabIcon} />
          <Text style={styles.tabTitle}> Ventas</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigate("Settings")}
        >
          <Icon name="settings" style={styles.tabIcon} />
          <Text style={styles.tabTitle}> Ajustes</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  tabBar: {
    height: 60,
    backgroundColor: "#303456",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    borderColor: "#E5E5E5",
    borderTopWidth: 0.5,
    paddingHorizontal: 5,
    elevation: 3
  },
  tabItem: {
    alignItems: "center"
  },
  tabIcon: {
    color: "#90a4ae",
    fontSize: 23,
    fontWeight: "100"
  },
  tabTitle: {
    marginTop: 3,
    fontSize: 10,
    color: "#90a4ae"
  },
  tabCart: {
    backgroundColor: "#f1c40f",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 55,
    width: 55,
    elevation: 3,
    marginTop: 0
  },
  tabCartIcon: {
    color: "black",
    fontSize: 26,
    fontWeight: "100"
  },
  tabCartTitle: {
    marginTop: 3,
    fontSize: 10,
    color: "#90a4ae"
  }
});
