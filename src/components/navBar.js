import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity
} from "react-native";

const NavBar = ({ navigation, user, store }) => {
  return (
    <SafeAreaView>
      <View style={styles.logoKomercia}>
        <Image
          source={require("../assets/iconkontrol.png")}
          style={{ width: 25, height: 25, marginRight: 6, borderRadius: 15 }}
        />
        <Text style={styles.textLogo}>Komercia</Text>
      </View>

      <View style={styles.navBar}>
        <Text style={styles.navBar_title}>{store.nombre}</Text>
        <TouchableOpacity onPress={() => navigation.navigate("Settings")}>
          <Image
            onPress={() => navigation.navigate("Settings")}
            style={styles.imageProfile}
            source={{ uri: `https://api2.komercia.co/users/${user.foto}` }}
          />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  navBar: {
    height: 25,
    flexDirection: "row",
    paddingHorizontal: 15,
    alignContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 0,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  logoKomercia: {
    flexDirection: "row",
    marginTop: 25,
    marginLeft: 15,
    alignItems: "center"
  },
  navBar_title: {
    marginLeft: 3,
    paddingTop: 4,
    fontWeight: "800",
    fontSize: 19,
    color: "#4c4c4c"
  },
  imageProfile: {
    width: 48,
    height: 48,
    borderRadius: 25,
    marginTop: -20
  },
  textLogo: {
    color: "#4c4c4c",
    fontSize: 14,
    fontWeight: "600"
  }
});

export default NavBar;
