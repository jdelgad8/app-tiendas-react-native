import React from "react";
import axios from "axios";
import { AsyncStorage } from "react-native";

const BASE_API = "https://api2.komercia.co/";
const CLIENT_SECRET = "S3kE1jYcd6hFWcu0jIOm3cRFMOnjjmtmtfoYdra1";
const CLIENT_ID = "2";
let ACCESS_TOKEN = "vacio";

class Api {
  _storeData = async token => {
    try {
      await AsyncStorage.setItem("userToken", token);
    } catch (error) {}
  };

  _storeDataIdStore = async token => {
    // try {
    //   await AsyncStorage.setItem("idStore", token);
    // } catch (error) {}
    return "431";
  };

  _retrieveData = async () => {
    // try {
    //   let token = await AsyncStorage.getItem("userToken");
    //     ACCESS_TOKEN = token;
    // } catch (error) {
    //   console.warn(error);
    // }

    ACCESS_TOKEN =
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjM1OGQ0YjA4ZmJlMzljNDc0MTczODY3NzQ5NjdiZDJhOWY3ZGZiYTBkNWI0ZTY5ZmI5OThiNDhkYTY5ZGZkOGQ3OTRkZTg1NWQyZDkwYjBiIn0.eyJhdWQiOiIyIiwianRpIjoiMzU4ZDRiMDhmYmUzOWM0NzQxNzM4Njc3NDk2N2JkMmE5ZjdkZmJhMGQ1YjRlNjlmYjk5OGI0OGRhNjlkZmQ4ZDc5NGRlODU1ZDJkOTBiMGIiLCJpYXQiOjE1NTU1MzIyNDAsIm5iZiI6MTU1NTUzMjI0MCwiZXhwIjoxNTU2ODI4MjQwLCJzdWIiOiIyMDciLCJzY29wZXMiOltdfQ.Bt9xqKgeWV0HDaZUXgBIYreHs_TUyOv4iaYsEuSqVzhFdamuGQDCuH58JY2NJvbZIuHRcAJViM42ZbN-5WCT3kAaFRMoyQR4wtLATIbn1ZFaa0qZ1vPSQEM9_SWzpx6eE8ZSsP0jztrXBikufrdQVXTyfAqKGur73hEDTdpkV0IqIu0n_Ag0UPNoVwnQsHk6P_ju_XDRNg0GJOyQ9sCk8PE-TWmmxfQHRxbqOPADrJo30hdYexO_fNOYV6TWbrqvTyW21Nfj8QQYkqix_-kpLKnwswoiT-sYE8OYcbmbkybn2wPdbZ6qcD7y-t7TbMjchsJmmsbiZaODuP1YKEHHDmt6WtiuI9lUmp6JbMSNJpuIpPc1f9gYnv7GzWFXXvIy0MQr84Fh5h09spKxrzJf_N1lMOcOWTf_4B1rFbe9lY3X56QlPZQQErifDwQNCz0Y5s409CKFlDINgnt9tNwfxA0JR32G6UTBFzksHwIurDLOjzbI26R_kg6BwiVt3MNdcpPC56PV0A_fGpmL-2PfcARpsexXECqiOtHh2qt3tYPe0tZYxDfQQim9hoNx0noqbr33nTvwj-HUkN6Lzz-bg6b2O8A8794yCEVSdlS0k_4Nl4GnQrG_iPWlQDY3A1IgTVMwauqeaeOWNMwXOBLB8IByfpSS9Srm-iW4pf76bXU";
  };

  _retrieveDataIdStore = async () => {
    try {
      let token = await AsyncStorage.getItem("userToken");
      ACCESS_TOKEN = token;
    } catch (error) {
      console.warn(error);
    }
  };

  postLogin(email, password) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${BASE_API}oauth/token`,
          {
            grant_type: "password",
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            password: password,
            username: email
          },
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          this._storeData(response.data.access_token);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
  // Información del usuario

  async getUser() {
    await this._retrieveData();
    const user = await axios
      .get(`${BASE_API}api/user`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return user;
  }

  // Información de la tienda

  async getStore() {
    await this._retrieveData();
    const store = await axios
      .get(`${BASE_API}api/admin/tienda`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        this._storeDataIdStore(response.data.id);
        return response.data.data;
      })
      .catch(error => error);
    return store;
  }

  // /api/admin/tienda
  // https://api2.komercia.co/logos/1-yVW0244.png

  async getInventory() {
    await this._retrieveData();
    const products = await axios
      .get(`${BASE_API}api/admin/productos`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return products;
  }

  async getProductDetails(idProduct) {
    await this._retrieveData();
    const products = await axios
      .get(`${BASE_API}/api/admin/productos/${idProduct}`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return products;
  }

  // TO-DO Create new product

  saveNewProduct(
    nombre,
    foto_cloudinary,
    id_foto,
    codigo_barras,
    descripcion,
    precio,
    unidades
  ) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${BASE_API}api/admin/productos`,
          {
            nombre: nombre,
            foto_cloudinary: foto_cloudinary,
            id_foto: id_foto,
            codigo_barras: codigo_barras,
            descripcion: descripcion,
            precio: precio,
            inventario: unidades,
            categoria_producto: 0,
            subcategoria: 0,
            activo: 1
          },
          {
            headers: {
              Authorization: `Bearer ${ACCESS_TOKEN}`
            }
          }
        )
        .then(response => {
          resolve(response);
        })
        .catch(errores => {
          console.warn("error en los datos");
          reject(errores);
        });
    });
  }

  // TO-DO Update product

  // TO-DO Delete Product

  // TO-DO show all sales

  async getSales() {
    await this._retrieveData();
    const sales = await axios
      .get(`${BASE_API}api/admin/ventas/ordenes`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return sales;
  }

  // TO-DO show sale details

  async getSaleDetails(idOrden) {
    const sales = await axios
      .get(`${BASE_API}api/admin/ventas/ordenes/${idOrden}`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return sales;
  }

  //TO-DO  Update Info Store

  //TO-DO Create new costumer

  //TO-Do Get all costumers

  async getCostumers() {
    const clients = await axios
      .get(`${BASE_API}api/gransan/clientes/tienda`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return clients;
  }
}

export default new Api();
