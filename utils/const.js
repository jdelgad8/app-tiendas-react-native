import React from "react";
import { Text, View } from "react-native";
import { Badge } from "native-base";

export const Bold = props => (
  <Text style={{ fontWeight: "bold" }}>{props.children}</Text>
);

export const Colors = {
  principal: "#4c4c4c",
  red: "#303456"
};

// 0.  Sin pagar
// 1.  Pagada
// 2.  Rechazada (Vendedor)
// 3.  Cancelado (Comprador)
// 4.  Despachado
// 5.  Recibido
// 6.  Finalizado y entregado
// 7.  Finalizado
// 8.  Cotizacion

export const SalesState = props => {
  let status = "";
  let color = "blue";
  let sale = props;
  switch (sale) {
    case "0":
      status = "Sin pagar";
      color = "#ffa801";
      break;
    case "1":
      status = "Pagada";
      color = "#6ab04c";
      break;
    case "2":
      status = "Rechazada";
      color = "#eb4d4b";
      break;
    case "3":
      status = "Cancelado";
      color = "#eb4d4b";
      break;
    case "4":
      status = "Despachado";
      color = "#4834d4";
      break;
    case "5":
      status = "Recibido";
      color = "#30336b";
      break;
    case "6":
      status = "Entregado";
      color = "#6ab04c";
      break;
    case "7":
      status = "Finalizado";
      color = "#00b894";
      break;
    case "8":
      status = "Cotización";
      color = "#706fd3";
      break;
  }
  return (
    <View>
      <Badge style={{ backgroundColor: color }}>
        <Text style={{ color: "white", padding: 3 }}>{status}</Text>
      </Badge>
    </View>
  );
};
