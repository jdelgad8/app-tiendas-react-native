import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  FlatList
} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import NavBar from "../src/components/navBar";
import TabBar from "../src/components/tabBar";
import API from "../utils/api";

export default class DashBoard extends React.Component {
  constructor() {
    super();
    this.state = {
      userData: "",
      storeData: "",
      inventory: "",
      feed: [
        {
          text: "Ventas",
          image:
            "https://i.pinimg.com/originals/48/a7/04/48a704e726dbaabbcbe998827cd613db.jpg"
        },
        {
          text: "Productos",
          image:
            "https://webgradients.com/public/webgradients_png/084%20Phoenix%20Start.png"
        },
        {
          text: "Visitas",
          image: "https://i.stack.imgur.com/dmWrH.png"
        }
      ]
    };
  }

  async componentDidMount() {
    const userData = await API.getUser();
    const storeData = await API.getStore();
    const products = await API.getInventory();
    this.setState({
      userData: userData,
      storeData: storeData,
      inventory: products.slice(0, 5),
      loading: false
    });
  }

  productViews(item) {
    if (item.length) {
      return (
        <Text style={{ paddingHorizontal: 10 }}>
          {" "}
          {item[0].numero_visitas}{" "}
        </Text>
      );
    } else {
      return <Text style={{ paddingHorizontal: 10 }}> 0 </Text>;
    }
  }

  getHorizontalFeed() {
    const { navigate } = this.props.navigation;
    return (
      <ImageBackground
        source={require("../src/assets/background.png")}
        style={styles.layoutFeed}
      >
        <Text style={styles.titleLayout}>Novedades</Text>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.state.feed}
          keyExtractor={(item, _) => item.image}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                navigate("DetailsProducts", {
                  product: item
                })
              }
            >
              <ImageBackground
                style={styles.feedContent}
                source={{
                  uri: item.image
                }}
              >
                <Text style={styles.textFeedContent}>{item.text}</Text>
              </ImageBackground>
            </TouchableOpacity>
          )}
        />
      </ImageBackground>
    );
  }

  getHorizontalInventory() {
    const { navigate } = this.props.navigation;
    return (
      <ImageBackground
        source={require("../src/assets/background.png")}
        style={styles.layoutFeed}
      >
        <Text style={styles.titleLayout}>Nuevos productos</Text>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.state.inventory}
          keyExtractor={(item, _) => item.nombre}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                navigate("DetailsProducts", {
                  product: item
                })
              }
            >
              <ImageBackground
                onPress={() =>
                  navigate("DetailsProducts", {
                    product: item
                  })
                }
                style={styles.InventoryContent}
                source={{
                  uri: this.fitImage(item.foto_cloudinary)
                }}
              />
              <Text
                style={{
                  color: "gray",
                  alignSelf: "center",
                  fontWeight: "bold",
                  color: "black"
                }}
              >
                <Icon name="eye" size={14} color="black" />
                {this.productViews(item.visitas_producto)}

                <Text style={{ fontWeight: "300", fontSize: 11 }}>vistas</Text>
              </Text>
            </TouchableOpacity>
          )}
        />
      </ImageBackground>
    );
  }

  fitImage(image) {
    let fitImage = image.split("/upload/");
    return (
      "https://res.cloudinary.com/komercia-store/image/upload/w_550,q_auto:best/" +
      fitImage[1]
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <ScrollView style={styles.body}>
          <NavBar
            user={this.state.userData}
            store={this.state.storeData}
            navigation={this.props.navigation}
          />
          <View style={styles.menu_container}>
            <View style={styles.layoutFeed}>{this.getHorizontalFeed()}</View>
            <View style={styles.layoutFeed}>
              {this.getHorizontalInventory()}
            </View>
            <View style={styles.toolsTextContent}>
              <Text style={styles.titleLayout}>Herramientas</Text>
            </View>
            <View style={styles.menu_buttons}>
              <TouchableOpacity
                style={styles.menuItem}
                onPress={() => navigate("NewProductInfo")}
              >
                <Icon name="camera" style={styles.menuIcon} />
                <Text style={styles.menuTitle}> Crear Producto</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.menuItem}
                onPress={() => navigate("ListProducts")}
              >
                <Icon name="box" style={styles.menuIcon} />
                <Text style={styles.menuTitle}> Inventario</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.menu_buttons}>
              <TouchableOpacity
                style={styles.menuItem}
                onPress={() => navigate("ListCostumers")}
              >
                <Icon name="users" style={styles.menuIcon} />
                <Text style={styles.menuTitle}> Clientes</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.menuItem}
                onPress={() => navigate("Sales")}
              >
                <Icon name="trending-up" style={styles.menuIcon} />
                <Text style={styles.menuTitle}> Mis Ventas</Text>
              </TouchableOpacity>
            </View>

            {/* <View style={styles.menu_buttons}>
              <TouchableOpacity
                style={styles.menuItem}
                onPress={() => navigate("Soon")}
              >
                <Icon name="settings" style={styles.menuIcon} />
                <Text style={styles.menuTitle}> Ajustes</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.menuItem}
                onPress={() => navigate("Soon")}
              >
                <Icon name="message-square" style={styles.menuIcon} />
                <Text style={styles.menuTitle}> Mensajes</Text>
              </TouchableOpacity>
            </View> */}

            <View style={styles.menu_buttons}>
              <TouchableOpacity
                style={styles.menuItem}
                onPress={() => navigate("Blog")}
              >
                <Icon name="book" style={styles.menuIcon} />
                <Text style={styles.menuTitle}> Blog</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.menuItem}
                onPress={() => navigate("Helpdesk")}
              >
                <Icon name="user" style={styles.menuIcon} />
                <Text style={styles.menuTitle}> Ayuda</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <TabBar navigation={this.props.navigation} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#fafafa"
  },

  body: {
    flex: 1,
    backgroundColor: "#fafafa"
  },
  menu_container: {
    marginBottom: 20
  },
  menu_buttons: {
    marginTop: 6,
    marginBottom: 8,
    flexDirection: "row",
    justifyContent: "space-around",
    paddingHorizontal: 10
  },
  menuItem: {
    backgroundColor: "#F1F1F2",
    width: "45%",
    height: 80,
    borderRadius: 5,
    elevation: 2,
    alignItems: "center",
    justifyContent: "center"
  },
  menu_button_bottom: {
    paddingTop: 6,
    paddingBottom: 6,
    backgroundColor: "#DCDEE5",
    flexDirection: "row",
    justifyContent: "space-around",
    paddingHorizontal: 10,
    elevation: 1,
    borderColor: "#E5E5E5",
    borderTopWidth: 0.5
  },
  menuNewSale: {
    backgroundColor: "#3F46AD",
    width: "95%",
    height: 45,
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    elevation: 1
  },
  menuIcon: {
    color: "#313456",
    fontSize: 30
  },
  menuTitle: {
    color: "#313456",
    marginTop: 5
  },
  menuIconSale: {
    color: "white",
    fontSize: 18,
    marginRight: 10
  },
  menuTitleSale: {
    color: "white",
    fontSize: 16
  },
  flatList: {
    flex: 1
  },
  feedContent: {
    width: 200,
    height: 80,
    borderRadius: 5,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10
  },
  textFeedContent: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    textShadowColor: "rgba(0,0,0, .75)",
    textShadowOffset: {
      width: 2,
      height: 2
    },
    textShadowRadius: 0
  },
  InventoryContent: {
    width: 110,
    height: 100,
    borderRadius: 5,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10,
    backgroundColor: "white"
  },
  textFeedContent: {
    color: "white",
    fontSize: 18,
    fontWeight: "bold",
    textShadowColor: "rgba(0,0,0, .75)",
    textShadowOffset: {
      width: 2,
      height: 2
    },
    textShadowRadius: 0
  },
  layoutFeed: {
    paddingVertical: 8,
    paddingHorizontal: 8
  },
  titleLayout: {
    color: "#4c4c4c",
    fontSize: 14,
    marginBottom: 10,
    fontWeight: "600"
  },
  toolsTextContent: {
    paddingHorizontal: 17,
    marginTop: 12,
    width: "100%"
  }
});
