import React from "react";
import {
  StyleSheet,
  View,
  Image,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Text, Button, Container } from "native-base";
import Icon from "react-native-vector-icons/Feather";

export default class NewSaleProductDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      showCostumer: false
    };
  }
  _toggleModal = () => {
    this.setState({ showCostumer: !this.state.showCostumer });
    setTimeout(() => this.setState({ showModal: false }), 3000);
  };
  addCostumer() {
    if (!this.state.showCostumer) {
      return (
        <TouchableOpacity
          style={styles.addCostumer}
          onPress={this._toggleModal}
        >
          <Icon name="plus-circle" style={styles.icon} />
          <Text>Agregar Cliente</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.costumerDetails}>
          <View>
            <Text style={styles.title}>Comprador</Text>
            <Text style={styles.nameProduct}>Nombre</Text>
            <Text>Juan Camilo Gonzalez</Text>
            <Text style={styles.nameProduct}>Email</Text>
            <Text>juancamilo@gmail.com</Text>
            <Text style={styles.nameProduct}>Telefono</Text>
            <Text>3124539289</Text>
          </View>
          <View style={styles.detailsLeft}>
            <Image
              style={styles.imageProfile}
              source={require("../../src/assets/profile.png")}
            />
            <Text style={styles.textPhotoProfile}>Comprador </Text>
          </View>
        </View>
      );
    }
  }

  render() {
    const { goBack } = this.props.navigation;
    const { navigate } = this.props.navigation;

    return (
      <Container>
        <ScrollView>
          <View style={styles.header}>
            <Button transparent onPress={() => goBack()}>
              <Icon name="arrow-left" color="black" size={30} />
            </Button>
          </View>

          <View style={styles.productDetails}>
            <Text style={styles.title}>Productos</Text>
            <View style={styles.itemList}>
              <View style={styles.infoProduct}>
                <Image
                  style={styles.imageProduct}
                  source={require("../../src/assets/camisa-example.jpg")}
                />
                <View style={styles.itemListText}>
                  <Text>Camisa roja a cuadros referencia 003 Levis</Text>
                  <Text style={styles.textQuantity}>$ 5000 X 8 unidades </Text>
                </View>
                <View style={styles.itemListPrice}>
                  <Text style={styles.priceProduct}>$ 100.000</Text>
                  <Text style={styles.nameProduct}>Total venta</Text>
                </View>
              </View>
            </View>
            <View style={styles.separator} />

            <View style={styles.itemList}>
              <View style={styles.infoProduct}>
                <Image
                  style={styles.imageProduct}
                  source={require("../../src/assets/camiseta-nueva.png")}
                />
                <View style={styles.itemListText}>
                  <Text>Camiseta deportiva</Text>
                  <Text style={styles.textQuantity}>$ 5000 X 8 unidades </Text>
                </View>
                <View style={styles.itemListPrice}>
                  <Text style={styles.priceProduct}>$ 100.000</Text>
                  <Text style={styles.nameProduct}>Total venta</Text>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.separator} />

          {this.addCostumer()}

          <View style={styles.contentTotal}>
            <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Subtotal</Text>
              <Text style={styles.textTotal}>$ 2.000.000</Text>
            </View>
            <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Impuestos</Text>
              <Text style={styles.textTotal}>$ 450.000</Text>
            </View>
            <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Descuentos</Text>
              <Text style={styles.textTotal}>$ 0</Text>
            </View>
            <View
              style={{ width: "100%", height: 1, backgroundColor: "#4834d4" }}
            />
            <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Total Venta</Text>
              <Text style={styles.priceTotal}>$ 2.450.000</Text>
            </View>
          </View>
        </ScrollView>
        <View style={styles.footer}>
          <Button
            onPress={() => navigate("SalePayment")}
            block
            style={{
              margin: 5,
              elevation: 4,
              marginHorizontal: 18,
              backgroundColor: "#4834d4"
            }}
          >
            <Text style={{ color: "white", fontSize: 20 }}>Pagar</Text>
          </Button>
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "white",
    height: 60,
    paddingHorizontal: 20,
    alignItems: "flex-start",
    justifyContent: "center"
  },
  productDetails: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    width: "100%",
    padding: 20,
    paddingTop: 5
  },
  addCostumer: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    width: "100%",
    padding: 20,
    alignItems: "center",
    flexDirection: "row"
  },
  salesDetails: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    width: "100%",
    padding: 20,
    paddingTop: 5,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  costumerDetails: {
    flex: 1,
    backgroundColor: "#FFF",
    width: "100%",
    padding: 20,
    marginBottom: 15,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  detailsLeft: {
    marginRight: 15,
    alignItems: "center"
  },
  contentTotal: {
    width: "100%",
    height: 150,
    backgroundColor: "#F1F1F2",
    justifyContent: "space-between",
    alignItems: "center",
    borderColor: "#E5E5E5",
    paddingHorizontal: 20,
    paddingVertical: 15
  },
  priceTotal: {
    fontSize: 22,
    color: "black"
  },
  textTotal: {
    color: "black",
    fontSize: 14
  },
  itemList: {
    width: "95%",
    flexDirection: "column",
    marginVertical: 15
  },
  infoProduct: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "100%"
  },
  itemListText: {
    marginLeft: 8,
    justifyContent: "center",
    width: "53%"
  },
  itemListPrice: {
    marginLeft: 8,
    justifyContent: "center",
    width: "33%"
  },
  nameProduct: {
    fontWeight: "400",
    color: "#7f8c8d",
    fontSize: 13
  },
  textQuantity: {
    fontWeight: "400",
    color: "#7f8c8d",
    fontSize: 13,
    color: "#4834d4"
  },
  textPhotoProfile: {
    marginTop: 5,
    fontWeight: "400",
    fontSize: 12,
    color: "#4834d4"
  },
  imageProfile: {
    width: 70,
    height: 70,
    borderRadius: 15
  },
  imageProduct: {
    width: 60,
    height: 60,
    borderRadius: 6,
    marginRight: 5
  },
  title: {
    fontSize: 20,
    color: "black",
    fontWeight: "600",
    marginBottom: 10
  },
  separator: {
    backgroundColor: "#ecf0f1",
    height: 1,
    width: "100%",
    marginVertical: 5
  },
  sectionTotal: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between"
  },
  icon: {
    fontSize: 20,
    marginRight: 6,
    color: "#d63031"
  }
});
