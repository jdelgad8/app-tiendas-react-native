import React from "react";
import { StyleSheet, View, Image, ScrollView, FlatList } from "react-native";
import { Text, Button, Container } from "native-base";
import Icon from "react-native-vector-icons/Feather";
import API from "../../utils/api";
import { SalesState } from "../../utils/const";

export default class NewSaleProductDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      products: "",
      infoSale: "",
      tiendaName: ""
    };
  }

  async componentDidMount() {
    const props = this.props.navigation.state.params.sale;
    const infoSale = await API.getSaleDetails(props.id);
    this.setState({
      infoSale: infoSale,
      tiendaName: infoSale.venta.tienda_venta.nombre,
      products: infoSale.productos,
      loading: false
    });
  }

  render() {
    const { goBack } = this.props.navigation;
    const props = this.props.navigation.state.params.sale;

    return (
      <Container>
        <ScrollView>
          <View style={styles.header}>
            <Button transparent onPress={() => goBack()}>
              <Icon name="arrow-left" color="black" size={30} />
            </Button>
          </View>

          <View style={styles.salesDetails}>
            <View>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.title}>Venta</Text>
                <View style={{ marginLeft: 8 }}>
                  {SalesState(props.estado)}
                </View>
              </View>
              <Text style={styles.nameProduct}>No. orden</Text>
              <Text># {props.id}</Text>
              <Text style={styles.nameProduct}>Fecha de venta</Text>
              <Text>{props.fecha}</Text>
              <Text style={styles.nameProduct}>Vendedor</Text>
              <Text>{this.state.tiendaName}</Text>
            </View>
            <View style={styles.detailsLeft}>
              <Image
                style={styles.imageProfile}
                source={require("../../src/assets/profile.png")}
              />
              <Text style={styles.textPhotoProfile}>Vendedor </Text>
            </View>
          </View>

          <View style={styles.costumerDetails}>
            <View>
              <Text style={styles.title}>Comprador</Text>
              <Text style={styles.nameProduct}>Nombre</Text>
              <Text>{props.usuario.nombre}</Text>
              <Text style={styles.nameProduct}>Email</Text>
              <Text>{props.usuario.email}</Text>
              <Text style={styles.nameProduct}>Telefono</Text>
              <Text>3124539289</Text>
              <Text style={styles.nameProduct}>Medio de pago</Text>
              <Text>Tarjeta de credito</Text>
            </View>
            <View style={styles.detailsLeft}>
              <Image
                style={styles.imageProfile}
                source={{
                  uri: `https://api2.komercia.co/users/${props.usuario.foto}`
                }}
              />
              <Text style={styles.textPhotoProfile}>Comprador </Text>
            </View>
          </View>

          <View style={styles.productDetails}>
            <Text style={styles.title}>Productos</Text>

            <FlatList
              style={styles.flatList}
              data={this.state.products}
              keyExtractor={(item, _) => item.producto.nombre}
              renderItem={({ item }) => (
                <View style={styles.itemList}>
                  <View style={styles.infoProduct}>
                    <Image
                      style={styles.imageProduct}
                      source={{
                        uri: item.producto.foto_cloudinary
                      }}
                    />
                    <View style={styles.itemListText}>
                      <Text>{item.producto.nombre}</Text>
                      <Text style={styles.textQuantity}>
                        $ {item.precio_producto} X {item.unidades} unidades
                      </Text>
                    </View>
                    <View style={styles.itemListPrice}>
                      <Text style={styles.priceProduct}>
                        $ {item.precio_producto * item.unidades}
                      </Text>
                      {/* <Text style={styles.nameProduct}>Total venta</Text> */}
                    </View>
                  </View>
                  <View style={styles.separator} />
                </View>
              )}
            />
          </View>

          <View style={styles.contentTotal}>
            {/* <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Subtotal</Text>
              <Text style={styles.textTotal}>$ 2.000.000</Text>
            </View> */}
            {/* <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Impuestos</Text>
              <Text style={styles.textTotal}>$ 450.000</Text>
            </View>
            <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Descuentos</Text>
              <Text style={styles.textTotal}>$ 0</Text>
            </View> */}
            {/* <View
              style={{ width: "100%", height: 1, backgroundColor: "#1abc9c" }}
            /> */}
            <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Total Venta</Text>
              <Text style={styles.priceTotal}>$ {props.total}</Text>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column"
  },
  header: {
    backgroundColor: "white",
    height: 60,
    paddingHorizontal: 20,
    alignItems: "flex-start",
    justifyContent: "center"
  },
  productDetails: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    width: "100%",
    padding: 20,
    paddingTop: 5
  },
  salesDetails: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    width: "100%",
    padding: 20,
    paddingTop: 5,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  costumerDetails: {
    flex: 1,
    backgroundColor: "#F1F1F2",
    width: "100%",
    padding: 20,
    marginBottom: 15,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  detailsLeft: {
    marginRight: 15,
    alignItems: "center"
  },
  contentTotal: {
    width: "100%",
    height: 55,
    backgroundColor: "#303456",
    justifyContent: "space-between",
    alignItems: "center",
    borderColor: "#E5E5E5",
    paddingHorizontal: 20,
    paddingVertical: 15
  },
  priceTotal: {
    fontSize: 22,
    color: "white"
  },
  textTotal: {
    color: "white",
    fontSize: 14
  },
  itemList: {
    width: "95%",
    flexDirection: "column",
    marginVertical: 15
  },
  infoProduct: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "100%"
  },
  itemListText: {
    marginLeft: 8,
    justifyContent: "center",
    width: "53%"
  },
  itemListPrice: {
    marginLeft: 8,
    justifyContent: "center",
    width: "33%"
  },
  nameProduct: {
    fontWeight: "400",
    color: "#7f8c8d",
    fontSize: 13
  },
  textQuantity: {
    fontWeight: "400",
    color: "#7f8c8d",
    fontSize: 13,
    color: "#4834d4"
  },
  textPhotoProfile: {
    marginTop: 5,
    fontWeight: "400",
    fontSize: 12,
    color: "#4834d4"
  },
  imageProfile: {
    width: 70,
    height: 70,
    borderRadius: 45
  },
  imageProduct: {
    width: 60,
    height: 60,
    borderRadius: 6,
    marginRight: 5
  },
  title: {
    fontSize: 20,
    color: "black",
    fontWeight: "600",
    marginBottom: 10
  },
  separator: {
    backgroundColor: "#ecf0f1",
    height: 1,
    width: "100%",
    marginVertical: 5
  },
  sectionTotal: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between"
  }
});
