import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  FlatList,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Button,
  Title,
  Tab,
  Tabs,
  TabHeading
} from "native-base";
import Icon from "react-native-vector-icons/Feather";
import API from "../../utils/api";
import { SalesState } from "../../utils/const";

export default class ListCostumers extends Component {
  constructor() {
    super();
    this.state = {
      salesData: "",
      loading: true
    };
  }

  async componentDidMount() {
    const salesData = await API.getSales();
    this.setState({
      salesData: salesData,
      loading: false
    });
  }
  header() {
    const { navigate } = this.props.navigation;
    return (
      <Header>
        <Left>
          <Button transparent onPress={() => navigate("Dash")}>
            <Icon name="arrow-left" color="white" size={20} />
          </Button>
        </Left>
        <Body>
          <Title>Ventas</Title>
        </Body>
        <Right>
          <Button transparent onPress={() => navigate("ListProductSales")}>
            <Text style={{ color: "white", fontSize: 13 }}>Nueva venta</Text>
            <Icon name="plus-circle" style={styles.iconHeader} />
          </Button>
        </Right>
      </Header>
    );
  }

  cotizaciones() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#f14b5a" />
        </View>
      );
    }
    return (
      <FlatList
        style={styles.flatList}
        data={this.state.salesData.cotizaciones}
        keyExtractor={(item, _) => item.fecha}
        renderItem={({ item }) => (
          <TouchableHighlight
            onPress={() =>
              navigate("DetailsProducts", {
                product: item
              })
            }
          >
            <List>
              <ListItem thumbnail>
                <Left style={{ width: 80 }}>
                  <Text style={styles.name}>{item.id}</Text>
                </Left>
                <Body
                  style={{
                    flexDirection: "row",
                    justifyContent: "flex-start",
                    alignItems: "center"
                  }}
                >
                  <Text style={{ width: 110 }}>{item.fecha}</Text>
                  {SalesState(item.estado)}
                </Body>

                <Right />
              </ListItem>
            </List>
          </TouchableHighlight>
        )}
      />
    );
  }

  ventas() {
    const { navigate } = this.props.navigation;
    return (
      <View>
        <FlatList
          style={styles.flatList}
          data={this.state.salesData.ventas}
          keyExtractor={(item, _) => item.fecha}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                navigate("SaleDetails", {
                  sale: item
                })
              }
            >
              <View style={styles.listItem}>
                <View style={{ flex: 2 }}>
                  <Text style={styles.name}># {item.id}</Text>
                </View>
                <View
                  style={{
                    flex: 5,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  {SalesState(item.estado)}
                  <Text style={{ marginLeft: 10 }}>12/10/2018</Text>
                </View>

                <View style={{ flex: 3, alignItems: "flex-end" }}>
                  <Text
                    style={{
                      fontWeight: "400",
                      fontSize: 16,
                      color: "black"
                    }}
                  >
                    $ {item.total}
                  </Text>
                </View>
              </View>
              <View style={styles.separator} />
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        {this.header()}
        <Content>
          <Tabs tabBarUnderlineStyle={{ backgroundColor: "#706fd3" }}>
            <Tab
              heading={
                <TabHeading style={{ backgroundColor: "white" }}>
                  <Text style={{ color: "black" }}>Ventas</Text>
                </TabHeading>
              }
            >
              {this.ventas()}
            </Tab>
            <Tab
              heading={
                <TabHeading style={{ backgroundColor: "white" }}>
                  <Text style={{ color: "black" }}>Cotizaciones</Text>
                </TabHeading>
              }
            >
              {this.cotizaciones()}
            </Tab>
          </Tabs>
        </Content>
        {/* 
        <View style={styles.contentTotal}>
          <Text style={styles.textTotal}>Total Ventas</Text>
          <Text style={styles.priceTotal}>$ 2.450.243</Text>
        </View> */}
        <View
          style={{ width: "100%", height: 2, backgroundColor: "#1abc9c" }}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    color: "blue",
    fontSize: 20
  },
  name: {
    color: "black",
    fontSize: 16
  },
  title: {
    color: "black",
    fontSize: 18,
    fontWeight: "bold"
  },
  contentTotal: {
    flexDirection: "row",
    height: 30,
    paddingHorizontal: 20,
    height: 40,
    backgroundColor: "#303456",
    justifyContent: "space-around",
    alignItems: "center",
    borderColor: "#E5E5E5",
    borderTopWidth: 0.5,
    paddingHorizontal: 5,
    elevation: 3
  },
  listItem: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
    marginTop: 15,
    paddingHorizontal: 14
  },
  priceTotal: {
    fontSize: 22,
    color: "white"
  },
  textTotal: {
    color: "white"
  },
  iconHeader: {
    color: "white",
    fontSize: 18,
    marginLeft: 6
  },
  separator: {
    backgroundColor: "#ecf0f1",
    height: 1,
    width: "100%",
    marginVertical: 5
  }
});
