import React from "react";
import { WebView } from "react-native";

export default class Terms extends React.Component {
  // static navigationOptions = {
  //   title: "Terminos y condiciones"
  // };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <WebView
        source={{
          uri:
            "https://drive.google.com/file/d/0By2Py0sG5nn3cWVlMnhHeWVVem8/preview"
        }}
      />
    );
  }
}
