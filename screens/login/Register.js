import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  ImageBackground
} from "react-native";

export default class Register extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.imageBack}
          source={{
            uri:
              "https://images.pexels.com/photos/590045/pexels-photo-590045.jpeg?w=940&h=650&auto=compress&cs=tinysrgb"
          }}
        >
          <View style={styles.header}>
            <Text style={{ fontSize: 29, fontWeight: "500" }}> Register</Text>
          </View>
          <View style={styles.social}>
            {/* <SocialIcon
                  style={styles.socialIcon}
                  title='Registrate con Facebook'
                  button
                  type='facebook'
                  onPress={() => navigate('Dash') }
                /> */}
          </View>
          <View style={styles.footer}>
            <Text style={{ color: "white" }}>
              Aceptas los terminos y condiciones
            </Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "column"
  },
  header: {
    flex: 8,
    alignContent: "center",
    justifyContent: "center"
  },
  social: {
    flex: 3
  },
  footer: {
    flex: 1
  },
  socialIcon: {
    padding: 30
  },
  imageBack: {
    flex: 1,
    width: "100%",
    height: "100%",
    padding: 20,
    justifyContent: "center",
    alignItems: "center"
  }
});
