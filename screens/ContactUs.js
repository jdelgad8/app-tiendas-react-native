import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, ImageBackground } from 'react-native';
import { Card, ListItem, Button, FlatList, Header, Divider, Icon, SocialIcon } from 'react-native-elements';
import { TabsFooter } from '../navigation/Router';

export default class DashBoard extends React.Component {

  render(){
    const { navigate } = this.props.navigation;
    return ( 
            
        <View style={styles.container}>
            <View>
                <Text>fdf</Text>
            </View>
            <Text>menu</Text>
            <TabsFooter/> 
        </View>             
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#26334F',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'column',
  },
  header:{
    flex: 8,
    alignContent: 'center',
    justifyContent: 'center'
  },
  social:{
    flex: 3
  },
  footer:{
    flex: 1,
  },
  socialIcon:{
    padding: 30,    
  },
  imageBack:{
    flex: 1,
	width:'100%',
    height: '100%',
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center'    
  },

});
