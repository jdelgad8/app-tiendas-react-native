import React from "react";
import { StyleSheet, Text, Image, View, WebView } from "react-native";
import { Container, Button } from "native-base";
import API from "../../utils/api";
import Icon from "react-native-vector-icons/Feather";
import Swiper from "react-native-web-swiper";

export default class DetailsProducts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productDetails: ""
    };
  }

  async componentDidMount() {
    let detailsProductsAPI = await API.getProductDetails(
      this.props.navigation.state.params.product.id
    );
    this.setState({
      productDetails: detailsProductsAPI
    });
  }

  fitImage(image) {
    let fitImage = image.split("/upload/");
    return (
      "https://res.cloudinary.com/komercia-store/image/upload/w_200,q_auto:best,f_auto/" +
      fitImage[1]
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    const item = this.props.navigation.state.params.product;
    const { goBack } = this.props.navigation;

    return (
      <Container>
        <View style={styles.contImage}>
          <View style={styles.header}>
            <Button transparent onPress={() => goBack()}>
              <Icon name="arrow-left" color="black" size={30} />
            </Button>
          </View>
          {/* <View style={styles.imageContent}>
            <Swiper>
              <View style={[styles.slideContainer, styles.slide1]}>
                <Text>Slide 1</Text>
              </View>
              <View style={[styles.slideContainer, styles.slide2]}>
                <Text>Slide 2</Text>
              </View>
              <View style={[styles.slideContainer, styles.slide3]}>
                <Text>Slide 3</Text>
              </View>
            </Swiper>
          </View>
          <Swiper
            autoplayTimeout={1}
            dotStyle={{ backgroundColor: "rgba(0,0,0,.2)" }}
            style={{ backgroundColor: "rgba(0,0,0,.2)", flex: 1 }}
          >
            <Image
              style={styles.imageProduct}
              source={{ uri: this.fitImage(item.foto_cloudinary) }}
            />
          </Swiper> */}
        </View>

        <View style={styles.contentDetails}>
          <Text style={styles.itemNombre}>{item.nombre}</Text>
          <Text style={styles.textStars}>
            <Icon name="star" size={15} color="#f1c40f" />{" "}
            <Icon name="star" size={15} color="#f1c40f" />{" "}
            <Icon name="star" size={15} color="#f1c40f" /> {"  "}Calificaciones
          </Text>
          <Text style={styles.priceProduct}>$ {item.precio}</Text>
          <Text>{item.inventario} unidades </Text>

          <Text style={{ color: "#27ae60" }}>
            <Icon name="eye" /> {item.visitas} Visitas
          </Text>
          <View style={styles.contentDescription}>
            <WebView
              originWhitelist={["*"]}
              source={{ html: "<h1>Hello world</h1>" }}
            />
          </View>
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  contImage: {
    width: "100%",
    flex: 2,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
    backgroundColor: "red"
  },
  header: {
    backgroundColor: "white",
    flex: 1,
    height: 60,
    width: "90%",
    alignItems: "flex-start",
    justifyContent: "center"
  },
  imageContent: {
    backgroundColor: "blue",
    flex: 2,
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  contentDetails: {
    width: "100%",
    height: "100%",
    backgroundColor: "#ecf0f1",
    padding: 15,
    flex: 2
  },
  itemNombre: {
    fontSize: 22,
    fontWeight: "100"
  },
  priceProduct: {
    fontWeight: "100",
    color: "#2c3e50",
    fontSize: 28
  },
  visitas: {
    color: "#7f8c8d"
  },
  textStars: {
    paddingVertical: 5,
    alignItems: "center"
  },
  contentDescription: {
    marginVertical: 10
  },
  slideContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },

  imageProductSwiper: {
    flex: 1,
    width: "100%",
    height: "100%",
    resizeMode: "cover",
    backgroundColor: "red"
  },
  imageProduct: {
    width: "85%",
    height: 240,
    backgroundColor: "white"
  },
  slideContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  slide1: {
    backgroundColor: "rgba(20,20,200,0.3)"
  },
  slide2: {
    backgroundColor: "rgba(20,200,20,0.3)"
  },
  slide3: {
    backgroundColor: "rgba(200,20,20,0.3)"
  }
});
