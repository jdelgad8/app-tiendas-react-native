import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Image
} from "react-native";
import {
  Button,
  Input,
  Item,
  Text,
  Body,
  Title,
  Header,
  Right,
  Container,
  Badge
} from "native-base";
import ImagePicker from "react-native-image-picker";
import Icon from "react-native-vector-icons/Feather";
import Modal from "react-native-modal";
import axios from "axios";
import API from "../../utils/api";

const options = {
  title: "Select Avatar",
  customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
  storageOptions: {
    skipBackup: false,
    path: "images"
  }
};

export default class NewProductInfo extends React.Component {
  constructor() {
    super();
    this.state = {
      idStore: "",
      tags: "No has subido ninguna imagen",
      barcode: null,
      name: "",
      idPhotoCloudinary: "",
      barCode: "",
      description: "",
      price: 0,
      units: 0,
      isModalVisible: false,
      showModalLoading: false,
      modalIcon: " ",
      modalMessage: " ",
      modalLink: " ",
      error: false,
      imageSourceExample:
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXp7vG6vsG3u77s8fTCxsnn7O/f5OfP09bFyczM0dO8wMPk6ezY3eDh5unJzdDR1tlr0sxZAAACVUlEQVR4nO3b23KDIBRA0QgmsaLx//+2KmPi/YJMPafZ6619sOzARJjq7QYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuJyN4+qMZcUri+BV3WQ22iIxSRTGFBITbRGpr218Ckx0EQPrxMfVPRP25QvNaT4xFTeJ1g/sJ4K8/aTuVxdNNJ99/Q0RQWlELtN7xGH9+8KYH1ZEX1hY770C9186Cm2R1TeONGj/paHQury7OwbsvzQUlp/9jakOJ2ooPLf/kl9on4Mtan50EhUUDvfgh8cqv/AxKlw+Cc3vPeUXjg+Kr4VCm+Vbl5LkeKHNTDKbKL9w3yr1B8q5RPmFu75puhPzTKKCwh13i2aJJguJ8gt33PG7GZxN1FC4tWvrB04TNRRu7Lw/S3Q2UUPh+ulpOIPTRB2FKyfgaeAoUUvhkvESnSYqL5ybwVGi7sKlwH6i6sL5JTpKVFZYlr0flmewTbyvX+piC8NyiXHvH9YD37OoqtA1v+wS15ZofxY1FTo/cJ+4NYNJd9BSVOi6kTeJOwLVFbrPyJ3dXqL6Cl1/7G7HDGordMOx7+hTVui2arQXBgVqKgwLVFQYGKinMDRQTWFwoJrC8AfcKLwUhRRSeL3vKkyDVaNLSdIf1snXEBQUyrlUTBQeIbPQD6uK8Zx3+yyHKbf/5N+y/gn78K/Rj/ZmY64Omhg9gHFaJu59i+EDGKf1/tshRxlxEoW+2uXS868EeflDYmDNltUzgkpqXyPGzULyK6QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8DV+AUrRI7QWHsWNAAAAAElFTkSuQmCC",
      imageSource:
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXp7vG6vsG3u77s8fTCxsnn7O/f5OfP09bFyczM0dO8wMPk6ezY3eDh5unJzdDR1tlr0sxZAAACVUlEQVR4nO3b23KDIBRA0QgmsaLx//+2KmPi/YJMPafZ6619sOzARJjq7QYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuJyN4+qMZcUri+BV3WQ22iIxSRTGFBITbRGpr218Ckx0EQPrxMfVPRP25QvNaT4xFTeJ1g/sJ4K8/aTuVxdNNJ99/Q0RQWlELtN7xGH9+8KYH1ZEX1hY770C9186Cm2R1TeONGj/paHQury7OwbsvzQUlp/9jakOJ2ooPLf/kl9on4Mtan50EhUUDvfgh8cqv/AxKlw+Cc3vPeUXjg+Kr4VCm+Vbl5LkeKHNTDKbKL9w3yr1B8q5RPmFu75puhPzTKKCwh13i2aJJguJ8gt33PG7GZxN1FC4tWvrB04TNRRu7Lw/S3Q2UUPh+ulpOIPTRB2FKyfgaeAoUUvhkvESnSYqL5ybwVGi7sKlwH6i6sL5JTpKVFZYlr0flmewTbyvX+piC8NyiXHvH9YD37OoqtA1v+wS15ZofxY1FTo/cJ+4NYNJd9BSVOi6kTeJOwLVFbrPyJ3dXqL6Cl1/7G7HDGordMOx7+hTVui2arQXBgVqKgwLVFQYGKinMDRQTWFwoJrC8AfcKLwUhRRSeL3vKkyDVaNLSdIf1snXEBQUyrlUTBQeIbPQD6uK8Zx3+yyHKbf/5N+y/gn78K/Rj/ZmY64Omhg9gHFaJu59i+EDGKf1/tshRxlxEoW+2uXS868EeflDYmDNltUzgkpqXyPGzULyK6QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8DV+AUrRI7QWHsWNAAAAAElFTkSuQmCC"
    };
  }

  async componentDidMount() {
    let idStoreAPI = await API._retrieveDataMembership();
    console.warn(idStoreAPI);
    this.setState({
      idStore: idStoreAPI
    });
  }

  returnDataBarCode = e => {
    this.setState({ barcode: e });
  };

  saveNewProduct() {
    //save photo on cloudinary
    let params = new FormData();
    params.append("file", this.state.imageSource);
    params.append("upload_preset", "qciyydun");
    params.append("folder", `${this.state.idStore}/products`);

    let config = {
      headers: {
        Accept: "application/json",
        "content-type": "multipart/form-data"
      }
    };
    axios
      .post(
        `https://api.cloudinary.com/v1_1/komercia-store/image/upload`,
        params,
        config
      )
      .then(response => {
        let photoCloudinary = response.data.secure_url;
        let idPhotoCloudinary = response.data.public_id;
        API.saveNewProduct(
          this.state.name,
          photoCloudinary,
          `${this.state.idStore}/products/${idPhotoCloudinary}`,
          this.state.barCode,
          this.state.description,
          this.state.price,
          this.state.units
        );
        this.setState({ showModalLoading: false });
      });
  }
  onpressSaveProduct() {
    this.setState({ showModalLoading: true });
    this.toggleModal();

    //Falta la imagen
    if (!this.state.imageSource) {
      this.setState({
        error: true,
        modalMessage: "ERROR, debes subir una imagen del producto"
      });
      this.setState({ showModalLoading: false });

      // Falta el nombre
    } else if (!this.state.name) {
      this.setState({
        error: true,
        modalMessage: "ERROR: Falta el nombre del producto"
      });
      this.setState({ showModalLoading: false });
    } else {
      this.setState({
        error: false,
        modalMessage: "El producto ha sido registrado"
      });
      this.saveNewProduct();
    }
  }
  toggleModal() {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }
  modal() {
    const { navigate } = this.props.navigation;
    if (this.state.showModalLoading) {
      return (
        <View style={styles.modalContent}>
          <View
            style={{
              paddingTop: 5,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <ActivityIndicator size="large" color="#f14b5a" />
            <Text>Guardando información</Text>
            <Text>Espere un momento por favor</Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.modalContent}>
          {this.state.error ? (
            <Image
              style={{ width: 50, height: 50, marginBottom: 5 }}
              source={require("../../src/assets/error.png")}
            />
          ) : (
            <Image
              style={{ width: 50, height: 50, marginBottom: 5 }}
              source={require("../../src/assets/check-mark.png")}
            />
          )}
          <Text style={styles.modalText}>{this.state.modalMessage}</Text>
          {this.state.error ? (
            <TouchableOpacity
              onPress={() => {
                this.setState({ isModalVisible: false });
              }}
              style={styles.finishButton}
            >
              <Text style={{ color: "white" }}>Continuar</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                this.setState({ isModalVisible: false }),
                  navigate("ListProducts");
              }}
              style={styles.finishButton}
            >
              <Text style={{ color: "white" }}>Continuar</Text>
            </TouchableOpacity>
          )}
        </View>
      );
    }
  }
  _galery() {
    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.warn("User cancelled image picker");
      } else if (response.error) {
        console.warn("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.warn("User tapped custom button: ", response.customButton);
      } else {
        let base64img = "data:image/jpeg;base64," + response.data;
        this.setState({
          imageSource: base64img
        });
      }
    });
  }
  _camera() {
    ImagePicker.launchCamera(options, response => {
      let base64img = "data:image/jpeg;base64," + response.data;
      this.setState({
        imageSource: base64img
      });
    });
  }
  _barcodeButton = () => {
    const { navigate } = this.props.navigation;
    if (this.state.barcode) {
      return (
        <View style={styles.barcodeAlreadyContent}>
          <View style={styles.barcodeAlready}>
            <Icon style={styles.barcodeAlreadyIcon} name="check-circle" />
            <Text style={styles.barcodeAlreadyText}>Código guardado</Text>
          </View>
          <Text style={styles.textBarcodeData}>{this.state.barcode}</Text>
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() =>
            navigate("NewProductBarCode", {
              returnData: this.returnDataBarCode
            })
          }
          style={styles.captureBarcode}
        >
          <Image
            style={{
              width: 38,
              height: 35,
              alignSelf: "center"
            }}
            source={require("../../src/assets/barcode.png")}
          />
          <Text style={{ color: "#303456", fontSize: 13 }}>Escanear</Text>
        </TouchableOpacity>
      );
    }
  };
  header() {
    const { navigate } = this.props.navigation;
    const { goBack } = this.props.navigation;
    return (
      <Header>
        <Body style={{ marginLeft: 12 }}>
          <Title>Registrar Producto</Title>
        </Body>
        <Right>
          <TouchableOpacity
            style={styles.cancelButton}
            onPress={() => goBack()}
          >
            <Text style={{ color: "white", fontSize: 13 }}>Cancelar</Text>
            <Icon name="x-circle" style={styles.icon} />
          </TouchableOpacity>
        </Right>
      </Header>
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Modal isVisible={this.state.isModalVisible}>{this.modal()}</Modal>

        {this.header()}

        <ScrollView style={styles.form}>
          <View style={styles.productTitleContent}>
            <Badge primary>
              <Text>1</Text>
            </Badge>
            <View style={styles.contentInputTitle}>
              <Text>
                Nombre del Producto{" "}
                <Text style={styles.required}>( Requerido )</Text>
              </Text>
              <Item style={styles.regularItem} regular>
                <Input onChangeText={name => this.setState({ name })} />
              </Item>
            </View>
          </View>

          <View style={styles.productTitleContent}>
            <Badge primary>
              <Text>2</Text>
            </Badge>
            <View style={styles.contentInputTitle}>
              <Text>
                Foto del Producto
                <Text style={styles.required}>( Requerido)</Text>
              </Text>
              <View style={styles.imageAndTextPhoto}>
                <View style={styles.contentIconPhoto}>
                  <TouchableOpacity
                    onPress={() => this._camera()}
                    style={styles.capture}
                  >
                    <Icon
                      style={{
                        color: "#303456",
                        marginBottom: 3,
                        fontSize: 25
                      }}
                      name="camera"
                    />
                    <Text style={{ color: "#303456", fontSize: 13 }}>
                      Tomar Foto
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this._galery()}
                    style={styles.capture}
                  >
                    <Icon
                      style={{
                        color: "#303456",
                        marginBottom: 3,
                        fontSize: 25
                      }}
                      name="grid"
                    />
                    <Text style={{ color: "#303456", fontSize: 13 }}>
                      Galería
                    </Text>
                  </TouchableOpacity>
                </View>
                <Image
                  source={{ uri: this.state.imageSource }}
                  style={styles.image}
                />
              </View>
            </View>
          </View>

          <View style={styles.productTitleContent}>
            <Badge primary>
              <Text>3</Text>
            </Badge>
            <View style={styles.contentInputTitle}>
              <Text>
                Código de Barras
                <Text style={styles.optional}>( Opcional )</Text>
              </Text>
              <Text style={{ fontSize: 12 }}>
                Escanea el código del producto
              </Text>
              <View style={styles.contentIconBarcode}>
                {this._barcodeButton()}
              </View>
            </View>
            <View style={{ height: 20 }} />
          </View>

          <View style={styles.productTitleContent}>
            <Badge primary>
              <Text>4</Text>
            </Badge>
            <View style={styles.contentInputTitle}>
              <Text>
                Descripción <Text style={styles.optional}>( Opcional )</Text>
              </Text>
              <Item style={styles.regularItem} regular>
                <Input
                  onChangeText={description => this.setState({ description })}
                />
              </Item>
              <Text>Precio de venta</Text>
              <Item style={styles.regularItem} regular>
                <Input onChangeText={price => this.setState({ price })} />
              </Item>
              <Text>Unidades disponibles</Text>
              <Item style={styles.regularItem} regular>
                <Input onChangeText={units => this.setState({ units })} />
              </Item>
            </View>
          </View>
        </ScrollView>
        <View style={styles.footer}>
          <Button
            onPress={() => this.onpressSaveProduct()}
            block
            style={{
              margin: 5,
              elevation: 4,
              marginHorizontal: 18,
              backgroundColor: "#3F46AD"
            }}
          >
            <Text style={{ color: "white" }}>Guardar</Text>
          </Button>
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column"
  },
  form: {
    width: "100%"
  },
  separator: {
    width: "100%",
    flexDirection: "column",
    backgroundColor: "#ffffff",
    padding: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  footer: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
    backgroundColor: "#ECEFF1",
    borderColor: "#E5E5E5",
    borderTopWidth: 0.5
  },
  form: {
    padding: 10,
    backgroundColor: "#F9F9F9"
  },
  productTitleContent: {
    padding: 8,
    flexDirection: "row",
    marginBottom: 2
  },
  contentInputTitle: {
    marginLeft: 10,
    width: "89%"
  },
  regularItem: {
    backgroundColor: "white",
    marginTop: 5,
    marginBottom: 10
  },
  checkedItem: {
    height: 45,
    width: "100%",
    flexDirection: "row",
    marginVertical: 5,
    marginLeft: -8,
    marginBottom: 20,
    justifyContent: "space-between",
    alignItems: "center"
  },
  contentIconPhoto: {
    flexDirection: "column",
    marginTop: 10,
    marginBottom: 6
  },
  contentIconPhoto: {
    flexDirection: "column",
    marginTop: 10,
    marginBottom: 1
  },
  contentIconBarcode: {
    flexDirection: "row",
    marginTop: 10,
    marginBottom: 6
  },
  image: {
    width: 130,
    height: 130,
    marginTop: 7,
    backgroundColor: "black",
    borderRadius: 5
  },
  imageAndTextPhoto: { flexDirection: "row" },
  capture: {
    backgroundColor: "#f1c40f",
    borderRadius: 5,
    padding: 6,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10,
    marginBottom: 10,
    width: 90,
    elevation: 2,
    borderColor: "black"
  },
  captureBarcode: {
    backgroundColor: "#f1c40f",
    borderRadius: 5,
    padding: 6,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10,
    width: 90,
    elevation: 2,
    borderColor: "black"
  },
  required: {
    fontSize: 10,
    color: "red"
  },
  optional: {
    fontSize: 10,
    color: "#0288D1"
  },
  icon: {
    color: "white",
    fontSize: 18,
    marginLeft: 6
  },
  cancelButton: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  barcodeAlready: {
    flexDirection: "row",
    alignItems: "center"
  },
  barcodeAlreadyText: {
    color: "white",
    fontSize: 14
  },
  textBarcodeData: {
    fontSize: 12,
    marginTop: 10,
    color: "white"
  },
  barcodeAlreadyIcon: {
    color: "#69F0AE",
    fontSize: 18,
    marginRight: 5
  },
  barcodeAlreadyContent: {
    width: 190,
    backgroundColor: "#303456",
    borderRadius: 5,
    padding: 9,
    paddingHorizontal: 20,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10
  },
  modalContent: {
    flexDirection: "column",
    backgroundColor: "white",
    borderRadius: 8,
    height: 190,
    justifyContent: "center",
    alignItems: "center"
  },
  modalIcon: {
    color: "gray",
    fontSize: 35,
    marginBottom: 10,
    marginTop: 10
  },

  finishButton: {
    backgroundColor: "#3F46AD",
    borderRadius: 2,
    padding: 12,
    paddingHorizontal: 90,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    margin: 15,
    width: 250,
    marginTop: 30
  }
});
