import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";

export default class Chat extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View>
          <Text>fdf</Text>
        </View>
        <Text>Chat</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#26334F",
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "column"
  },
  header: {
    flex: 8,
    alignContent: "center",
    justifyContent: "center"
  },
  social: {
    flex: 3
  },
  footer: {
    flex: 1
  },
  socialIcon: {
    padding: 30
  },
  imageBack: {
    flex: 1,
    width: "100%",
    height: "100%",
    padding: 20,
    justifyContent: "center",
    alignItems: "center"
  }
});
