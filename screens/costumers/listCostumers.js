import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  FlatList
} from "react-native";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,
  Button,
  Title
} from "native-base";
import Icon from "react-native-vector-icons/Feather";
import API from "../../utils/api";

export default class ListCostumers extends Component {
  constructor() {
    super();
    this.state = {
      costumers: "",
      loading: true
    };
  }

  // static navigationOptions = {
  //   headerTitle: <Text>Clientes</Text>,
  //   headerRight: (
  //     <Button
  //       onPress={() => alert("This is a button!")}
  //       title="Info"
  //       color="red"
  //     />
  //   )
  // };

  async componentDidMount() {
    const costumers = await API.getCostumers();
    this.setState({
      costumers: costumers.clientes,
      loading: false
    });
  }

  header() {
    const { navigate } = this.props.navigation;
    return (
      <Header>
        <Left>
          <Button transparent onPress={() => navigate("Dash")}>
            <Icon name="arrow-left" color="white" size={20} />
          </Button>
        </Left>
        <Body>
          <Title>Mis Clientes</Title>
        </Body>
        <Right>
          <Button transparent onPress={() => navigate("NewCostumer")}>
            <Text style={{ color: "white", fontSize: 13 }}>Nuevo</Text>
            <Icon name="plus-circle" style={styles.iconHeader} />
          </Button>
        </Right>
      </Header>
    );
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <Container>
        {this.header()}
        <Content>
          <FlatList
            style={styles.flatList}
            data={this.state.costumers}
            keyExtractor={(item, _) => item.nombre}
            renderItem={({ item }) => (
              <ListItem onPress={() => navigate("DetailsCostumer")} thumbnail>
                <Left>
                  <Thumbnail
                    circle
                    source={{
                      uri: `https://api2.komercia.co/users/${item.foto}`
                    }}
                  />
                </Left>
                <Body>
                  <Text style={styles.name}>{item.nombre}</Text>
                  <Text note numberOfLines={1}>
                    Ultima compra: 3 meses
                  </Text>
                </Body>
                <Right>
                  <Icon name="arrow-right" style={styles.icon} />
                </Right>
              </ListItem>
            )}
          />
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    color: "blue",
    fontSize: 20
  },
  name: {
    color: "black",
    fontSize: 14
  },
  iconHeader: {
    color: "white",
    fontSize: 18,
    marginLeft: 6
  }
});
