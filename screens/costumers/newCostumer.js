import React, { Component } from "react";
import { StyleSheet, View, ScrollView } from "react-native";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  Title,
  Form,
  Item,
  Label,
  Input
} from "native-base";
import Icon from "react-native-vector-icons/Feather";

export default class NewCostumers extends Component {
  header() {
    const { navigate } = this.props.navigation;
    return (
      <Header>
        <Left>
          <Button transparent onPress={() => navigate("ListCostumers")}>
            <Icon name="arrow-left" color="white" size={20} />
          </Button>
        </Left>
        <Body>
          <Title>Registrar Cliente</Title>
        </Body>
      </Header>
    );
  }
  render() {
    return (
      <Container>
        {this.header()}
        <ScrollView style={styles.form}>
          <Form>
            <Item floatingLabel>
              <Label>Nombre </Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Apellido</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Telefono</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Email</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Tipo de documento</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label>Numero de documento</Label>
              <Input />
            </Item>
          </Form>
        </ScrollView>
        <View style={styles.footer}>
          <Button block style={{ margin: 10 }}>
            <Text>Guardar</Text>
          </Button>
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  capture: {
    flex: 0,
    backgroundColor: "#fff",
    borderRadius: 5,
    color: "#000",
    padding: 10,
    margin: 40
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column"
  },
  form: {
    width: "100%"
  },
  separator: {
    width: "100%",
    flexDirection: "column",
    backgroundColor: "#ffffff",
    padding: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  footer: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    elevation: 5
  }
});
